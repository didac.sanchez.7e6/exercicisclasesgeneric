﻿using System;
using System.Collections.Generic;

namespace JustFeed
{
    /*--- Classes ---*/
    public class ColaReparto : Queue<Pedido>
    {
        /*--- Atrivuto ---*/
        Queue<Pedido> Cola { get; set; }

        /*--- Constructor ---*/
        public ColaReparto()
        {
            Cola = new Queue<Pedido>();
        }
        /*--- Metodos ---*/
        public void AgregarPedido(string nombre, string tipo, string plato)
        {
            Cola.Enqueue(new Pedido(nombre, tipo, plato));
        }
        public string MostrarProximoPedido()
        {
            return Cola.Peek().ToString();
        }
        public void EliminarProximoPedido()
        {
            Cola.Dequeue().ToString();
        }
        public string SolicitudesPendientes()
        {
            string solicitudes = "";
            foreach (Pedido solicitud in Cola)
            {
                solicitudes += solicitud + "\n";
            }
            return solicitudes;
        }
    }
    public class Pedido
    {
        /*--- Atrivuto ---*/
        public string NombreCliente { get; set; }
        public string Direccion { get; set; }
        public string Pedir { get; set; }

        /*--- Constructor ---*/
        public Pedido(string pedir, string direcion, string nombre)
        {
            Pedir = pedir;
            Direccion = direcion;
            NombreCliente = nombre;
        }
        /*--- Metodo ---*/
        public override string ToString()
        {
            return $"{NombreCliente}: {Pedir} (Direcion {Direccion})";
        }
    }
    internal class JustFeed
    {
        static void Menu()
        {
            ColaReparto pedido = new ColaReparto();
            string opcio;
            string[] titulos = { "Exit", "Afagir Pedido" };
            do
            {
                foreach (string titulo in titulos)
                {
                    Console.WriteLine(titulo);
                }
                opcio = MenuOpcions(AskString("Quiena opsio vol?"), pedido);
                Console.Clear();
                Console.WriteLine($"Las solistuts que tens per atendre\n{pedido.SolicitudesPendientes()}");
            } while (opcio != "0");

        }
        static string MenuOpcions(string opcio, ColaReparto solicituds)
        {
            switch (opcio)
            {
                case "0":
                    return opcio;
                case "1":
                    solicituds.AgregarPedido(AskString("Nom del client"), AskString("Direcion"), AskString("Que quiere"));
                    if (AskString($"Quiere borrar el sigiente pedido:\n{solicituds.MostrarProximoPedido()}\nSi\tNo").ToLower() == "si") solicituds.EliminarProximoPedido();
                    return opcio;
                default:
                    Console.Clear();
                    return opcio;
            }
        }
        static string AskString(string pregunta)
        {
            Console.WriteLine(pregunta);
            return Console.ReadLine();
        }

        static void Main()
        {
            Menu();
        }
    }
}
