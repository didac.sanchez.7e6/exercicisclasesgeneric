﻿using System;
using System.Collections.Generic;

namespace Exercicis_Clases_Generic.Collections
{
    /*--- Classes ---*/
    public class Solicitud
    {
        /*--- Atrivutos ---*/
        public string NombreCliente { get; set; }
        public string TipoSolicitud { get; set; }
        /*--- Constructor ---*/
        public Solicitud(string nombre, string tipo)
        {
            NombreCliente = nombre;
            TipoSolicitud = tipo;
        }
        /*--- Metodo ---*/
        public override string ToString()
        {
            return $"{NombreCliente}: {TipoSolicitud}";
        }
    }
    public class ColaCliente : Queue<Solicitud>
    {
        /*--- Atrivuto ---*/
        Queue<Solicitud> Cola { get; set; }
        /*--- Constructor ---*/
        public ColaCliente()
        {
            Cola = new Queue<Solicitud> { };
        }
        /*--- Metodos ---*/
        public void AgregarSolicitud(string nombre, string tipo)
        {
            Cola.Enqueue(new Solicitud(nombre, tipo));
        }
        public string MostrarSiguienteSolicitud()
        {
            return Cola.Peek().ToString();
        }
        public void EliminarSolicitudAtendida()
        {
             Cola.Dequeue().ToString();
        }
        public string SolicitudesPendientes()
        {
            string solicitudes = "";
            foreach (Solicitud solicitud in Cola)
            {
                solicitudes += solicitud + "\n";
            }
            return solicitudes;
        }
    }
    class Banca
    {
        static void Menu()
        {
            ColaCliente solicituds = new ColaCliente();
            string opcio;
            string[] titulos = { "Exit", "Afagir Solisitut" };
            do
            {
                foreach (string titulo in titulos)
                {
                    Console.WriteLine(titulo);
                }
                opcio = MenuOpcions(AskString("Quiena opsio vol?"), solicituds);
                Console.Clear();
                Console.WriteLine($"Las solistuts que tens per atendre\n{solicituds.SolicitudesPendientes()}");
            } while (opcio != "0");

        }
        static string MenuOpcions(string opcio, ColaCliente solicituds)
        {
            switch (opcio)
            {
                case "0":
                    return opcio;
                case "1":
                    solicituds.AgregarSolicitud(AskString("Nom del client"), AskString("Tipo de la solisitud"));
                    if (AskString($"Has completat la solistud\n{solicituds.MostrarSiguienteSolicitud()}\nSi\tNo").ToLower() == "si") solicituds.EliminarSolicitudAtendida();
                    return opcio;
                default:
                    Console.Clear();
                    return opcio;
            }
        }
        static string AskString(string pregunta)
        {
            Console.WriteLine(pregunta);
            return Console.ReadLine();
        }

        static void Main()
        {           
            Menu();
        }
    }
}
