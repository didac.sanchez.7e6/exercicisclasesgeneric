﻿using System;
using System.Collections.Generic;

namespace Yu_Gi_Oh_
{
    /*--- Classes ---*/
    public class Carta
    {
        /*--- Atrivutos ---*/
        public string Name { get; set; }
        public string Tipo { get; set; }
        public int Nivel { get; set; }
        public string Efecto { get; set; }

        /*--- Constructor ---*/
        public Carta(string name, string tipo, string efecto, int nivel)
        {
            Name = name;
            Tipo = tipo;
            Efecto = efecto;
            Nivel = nivel;
        }
        /*--- Metodo ---*/
        public override string ToString()
        {
            return $"{Name}: {Efecto} (Nivel {Nivel}, Tipo {Tipo})";
        }
    }
    public class MazoCartas : Stack<Carta>
    {
        /*--- Atrivutos ---*/
        Stack<Carta> Mazo { get; set; }
        /*--- Constructor ---*/
        public MazoCartas()
        {
            Mazo = new Stack<Carta>();
        }
        /*--- Metodos ---*/
        public void AgregarCarta(string name, string tipo, string efecto, int nivel)
        {
            Mazo.Push(new Carta(name, tipo, efecto, nivel));
        }
        public Carta RobarCarta()
        {
            return Mazo.Pop();
        }
        public bool EstaVacio()
        {
            return Mazo.Peek() == null;
        }
    }
    public class ManoJugador
    {
        /*--- Atrivutos ---*/
        public List<Carta> Mano { get; set; }
        /*--- Constructor ---*/
        public ManoJugador()
        {
            Mano = new List<Carta>();
        }
        /*--- Metodos ---*/
        public void AgregarCarta(MazoCartas Mazo)
        {
            Mano.Add(Mazo.RobarCarta());
        }
        public string JugarCarta(Carta carta)
        {
            string efecto = $"Has jugado la carta {carta.Name}, con el efecto:\n {carta.Efecto}";
            Mano.Remove(carta);
            return efecto;
        }
        public string DescartarCarta(Carta carta)
        {
            string efecto = $"Has descartado la carta {carta.Name}";
            Mano.Remove(carta);
            return efecto;
        }
        public bool EstaVacio()
        {
            return Mano.Count == 0;
        }
    }
    internal class YuGiOh
    {
        static void Main()
        {
            MazoCartas mazo = new MazoCartas();
            mazo.AgregarCarta("Bergamota", "Fuego", "Mientras tus LP sean más altos que los de tu adversario, si un monstruo de Tipo Planta que controles ataca a un monstruo en Posición de Defensa, inflige daño de batalla de penetración a tu adversario. Una vez por turno, si ganas LP: hasta el final del turno de tu adversario, esta carta gana 1000 ATK y DEF.", 6);
            mazo.AgregarCarta("Cananga", "Tierra", "Mientras tus LP sean más altos que los de tu adversario, los monstruos boca arriba que controle tu adversario pierden 500 ATK y DEF. Una vez por turno, si ganas LP: selecciona 1 Carta Mágica/de Trampa que controle tu adversario; devuélvela a la mano.", 3);
            mazo.AgregarCarta("Jazmín", "Luz", "Durante tu Main Phase, mientras tus LP sean más altos que los de tu adversario, puedes Invocar de Modo Normal 1 monstruo Planta además de tu Invocación Normal/Colocada, excepto \"Arobruja Jazmín\". (Sólo puedes ganar este efecto una vez por turno). Una vez por turno, si ganas LP: roba 1 carta.", 2);
            mazo.AgregarCarta("Romero", "Agua", "Mientras tus LP sean más altos que los de tu adversario, si un monstruo Planta que controles ataca, hasta el final del Damage Step tu adversario no puede activar efectos de monstruos. Una vez por turno, si ganas LP: selecciona 1 monstruo boca arriba en el Campo; cambia su posición de batalla.", 4);
            mazo.AgregarCarta("Mejorana", "Oscuridad", "Mientras tus LP sean mayores que los de tu adversario, no recibes daño de batalla de ataques que involucren tus monstruos Planta. Sólo puedes usar cada uno de los siguientes efectos de \"Arobruja Mejorana\" una vez por turno.\r\n●Cuando un monstruo Planta que controlas es destruido en batalla: puedes Invocar esta carta de Modo Especial desde tu mano, y después gana 500 LP.\r\n●Si ganas LP: selecciona cartas en el Cementerio de tu adversario, hasta el número de monstruos \"Arobru\" que controlas; destiérralas.", 5);
            ManoJugador jugador = new ManoJugador();
            jugador.AgregarCarta(mazo);
            jugador.AgregarCarta(mazo);
            jugador.Mano.ForEach(m => Console.WriteLine(m.ToString()));
        }
    }
}


